export default {
    data () {
        return {
            options: [
                {
                    id: 1,
                    name: 'Elemento 1'
                },
                {
                    id: 2,
                    name: 'Elemento 2'
                },
                {
                    id: 3,
                    name: 'Elemento 3'
                },
                {
                    id: 4,
                    name: 'Elemento 4'
                },
                {
                    id: 5,
                    name: 'Elemento 5'
                },
                {
                    id: 6,
                    name: 'Elemento 6'
                },
            ]
        }
    }
}
